FROM eclipse-temurin:17-jre-alpine
COPY backend/target/*.jar server.jar
EXPOSE 8080
CMD java -jar server.jar

# OPD Online (Mittelmaß)

A simple web app to automatically enter and calculate the points for adjudicating debates in the open parliamentary debate style (OPD)

A running version of this application can be found at https://opd.krapp.io


## Acknowledgements

Great thanks to Josef Hoppe and Georg Wicke-Arndt for their work in developing the backend adapted for use in this project (originally developed for whohere (whohere.hoppe.io))


## License

[MIT](https://choosealicense.com/licenses/mit/)


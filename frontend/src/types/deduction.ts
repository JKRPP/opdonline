import {SpeakerAffiliation} from "./teamTypes";

/** Deductions are used to apply penalties and consist of a name and a penalty value */
export type Deduction = {
  /** The name of the Deduction and also the identifier for it */
  name: string,
  /**
   * The penalty given to the speaker points
   *
   * Should be negative
   */
  penalty: number
}

/** Speakers can receive Deductions during a debate for violating the rules */
export type SpeakerDeduction = {
  position: number,
  affiliation: SpeakerAffiliation,
  deductions: Deduction[]
}

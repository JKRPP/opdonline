/**
 * An encoding maps number intervals to values
 *
 * The syntax is defined by using the upper bound and the value for that bound
 *
 * IMPORTANT: numbers need to be in ascending order for it to work properly
 *
 * e.g. [
 *    [2, "red"], // values <= 2 are red
 *    [7, "yellow"], // values between 3 and 7 are yellow
 *    [10, "green"], // values between 8 and 10 are yellow
 * ]
 */
export type Encoding<T> = ({ bound: number, value: T })[]

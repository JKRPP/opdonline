import {InteractionCategory, SpeakerCategory} from "./categories";
import {SpeakerAffiliation} from "./teamTypes";
import {Deduction} from "./deduction";

/**
 * A score is number which might be undefined if not present
 */
export type Score = (number | undefined)

export type Result = {
  /**
   *  Scores sorted by judge id
   */
  scores: Score[],
  /** Average of judge scores stored for convenience */
  average: number,
  /** Spread of judge scores stored for convenience */
  spread: number
}

export type ResultWithDeductions = Result & {
  /** Sum of the deductions stored for convenience */
  deductionSum: number,
  /** The final score after subtracting the deductions from the average */
  finalScore: number
}

export type SpeakerResult = ResultWithDeductions & {
  position: number,
  affiliation: SpeakerAffiliation,
  subcategories: Record<SpeakerCategory, Result>,
}

export type StrategyResult = Result & {
  speakers: Result[]
}

export type InteractionResult = Result & {
  subcategories: Record<InteractionCategory, Result>
}

export type AffiliationResult = {
  speakers: SpeakerResult[],
}

export type TeamResult = AffiliationResult & {
  strategy: StrategyResult,
  interaction: InteractionResult,
  persuasion: Result,
  teamSpeakerTotal: ResultWithDeductions,
  teamCategoriesTotal: Result,
  teamTotal: ResultWithDeductions,
}

export type DebateResult = {
  government: TeamResult,
  opposition: TeamResult,
  freeSpeeches: AffiliationResult,
}

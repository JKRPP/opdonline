import {SpeakerCategory, TeamCategoryWithoutStrategy} from "./categories";
import {SpeakerAffiliation} from "./teamTypes";
import {Judge} from "./judge";
import {Deduction, SpeakerDeduction} from "./deduction";


export type SpeakerCategoryAdjudication = Record<SpeakerCategory, number>

export type SpeakerAdjudication = {
  /** The position the speaker has in their affiliation */
  position: number,
  /** The name of the speaker */
  name?: string,
  /** The scores given to the speaker */
  categoryScore: SpeakerCategoryAdjudication,
  /** Should the scores for the categories be evaluated? */
  shouldEvaluateCategories: boolean,
  /**
   * The points a speaker gets for their team strategy contribution
   * This value is ignored for free speeches
   */
  strategy: number,
}

export type TeamAdjudication = {
  speakers: SpeakerAdjudication[]
  teamCategories: Record<TeamCategoryWithoutStrategy, number>,
  shouldEvaluateStrategy: boolean,
  shouldEvaluateTeamCategories: boolean,
  shouldEvaluatePersuasiveness: boolean
}

export type DebateAdjudication = {
  judge: Judge
  government: TeamAdjudication,
  opposition: TeamAdjudication,
  freeSpeeches: TeamAdjudication,
}


export type Debate = {
  adjudications: DebateAdjudication[]
  deductions: SpeakerDeduction[]
}





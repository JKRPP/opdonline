export const views = [
  "start",
  "roomSetup",
  "government",
  "opposition",
  "freeSpeeches",
  "evaluation",
  "results"
] as const

export type View = typeof views[number]

export const subviews = ["speaker", "strategy", "teamCategory"] as const

export type SubView = typeof subviews[number]

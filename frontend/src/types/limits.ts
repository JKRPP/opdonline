/**
 * A type for giving a lower an upper bound
 */
export type Limits = [number, number]

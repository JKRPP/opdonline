export const onlyWhenEvaluated = <T, >(value: T, evaluated: boolean, defaultValue: T) => evaluated ? value : defaultValue
export const onlyWhenEvaluatedNumber = (value: number, evaluated: boolean) => onlyWhenEvaluated(value, evaluated, 0)

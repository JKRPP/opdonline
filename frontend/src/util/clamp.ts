import {Limits} from "../types/limits";

export const clamp = (value: number, limits: Limits) => {
  return Math.max(limits[0], Math.min(value, limits[1]))
}

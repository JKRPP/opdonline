/**
 * @param from The value from which to start the range list
 * @param to The value to which the list should extend (exclusive)
 */
export const range = (from: number, to: number) => Array.from({length: to - from}, (_, i) => i + from)

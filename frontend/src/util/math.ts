export const sum = (...values: number[]) => {
  let result = 0
  values.forEach(value => result += value)
  return result
}

export const round = (value: number, decimalsAfterComma: number = 0) => {
  return Math.round(value * Math.pow(10, decimalsAfterComma)) / Math.pow(10, decimalsAfterComma)
}

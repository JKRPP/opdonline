export const deduplicate = <T,>(array: T[]): T[] => {
  let result: T[] = []
  for(let x of array){
    if(!result.find(value => value === x)){
      result.push(x)
    }
  }
  return result
}

import {useTranslation} from "../localization/useTranslation";
import {useResult} from "../util/hooks/useResult";
import {tw, tx} from "@twind/core";
import {Tag} from "./basic/Tag";
import {SpeakerAffiliation} from "../types/teamTypes";
import {createTab} from "../util/results";
import {SpeakerResult} from "../types/results";
import {useContext} from "react";
import {encodingMapper} from "../util/encodingMapper";
import {LoadingDisplay} from "./LoadingDisplay";
import {AppContext} from "../appContext";

type SpeakerResultDisplayProps = {
  name: string,
  score: number,
  position: number,
  affiliation: SpeakerAffiliation
}

const SpeakerResultDisplay = ({name, score, position, affiliation}: SpeakerResultDisplayProps) => {
  const {config} = useContext(AppContext)

  return (
    <Tag coloring={{color: affiliation, hover: false}} isRow={true}
         className={tw("w-full !justify-between items-center")}>
      <div className={tw("flex flex-row items-center gap-x-2")}>
        <Tag
          className={tx(
            "font-bold !py-1", {
              "!bg-[#FFD700]": position === 1,
              "!bg-[#C0C0C0]": position === 2,
              "!bg-[#CD7F32]": position === 3
            })}
          coloring={{color: "white", hover: false}}
        >
          {`${position}.`}
        </Tag>
        <span className={tw("font-bold")}>{name}</span>
      </div>
      <Tag coloring={{
        ...encodingMapper(score, config.totalSpeakerScoreColorEncoding),
        hover: false
      }}>{score.toFixed(2)}</Tag>
    </Tag>
  )
}

export type SpeakerSorting = "team" | "points" | "debateOrder"

export type SpeakerTabProps = {
  sorting?: SpeakerSorting,
  className?: string
}

export const SpeakerTab = ({sorting, className}: SpeakerTabProps) => {
  const translation = useTranslation()
  const result = useResult()
  const {config} = useContext(AppContext)

  if (!result) {
    return <LoadingDisplay/>
  }

  const tab = createTab(result)
  const scores = tab.map(value => value.finalScore).sort((a, b) => a > b ? -1 : 1)
  const speakerPositionSorting = (a: SpeakerResult, b: SpeakerResult) => a.position < b.position ? -1 : 1
  const govSpeeches = tab.filter(value => value.affiliation === "government").sort(speakerPositionSorting)
  const oppSpeeches = tab.filter(value => value.affiliation === "opposition").sort(speakerPositionSorting)
  const freeSpeeches = tab.filter(value => value.affiliation === "freeSpeeches").sort(speakerPositionSorting)

  let usedSorting: SpeakerResult[] = [];
  switch (sorting) {
    case "points":
      const scoreSorting = tab.sort((a: SpeakerResult, b: SpeakerResult) => a.finalScore > b.finalScore ? -1 : 1)
      usedSorting = scoreSorting;
      break
    case  "team":
      usedSorting = [
        ...govSpeeches,
        ...oppSpeeches,
        ...freeSpeeches
      ]
      break
    case  "debateOrder":
      const debateOrderSorting: SpeakerResult[] = []
      for (let i = 0; i < Math.max(config.teamSpeechCount - 1, 1); i++) {
        debateOrderSorting.push(govSpeeches[i])
        debateOrderSorting.push(oppSpeeches[i])
      }
      debateOrderSorting.push(...freeSpeeches)
      debateOrderSorting.push(oppSpeeches[oppSpeeches.length - 1])
      debateOrderSorting.push(govSpeeches[govSpeeches.length - 1])
      usedSorting = debateOrderSorting;
      break
  }

  return (
    <div className={tx("flex flex-col gap-y-2 w-full", className)}>
      {usedSorting.map(({affiliation, finalScore, position}) => (
        <SpeakerResultDisplay
          key={affiliation + position}
          name={`${translation[affiliation]} ${translation.speech(position)}`}
          score={finalScore}
          position={scores.findIndex(value => value === finalScore)! + 1}
          affiliation={affiliation}
        />
      ))}
    </div>
  )
}

import {useTranslation} from "../localization/useTranslation";
import {ViewBase} from "./ViewBase";
import {tw} from "@twind/core";
import {BookHeart, MessageCircleQuestion, Presentation, Speech} from "lucide-react";
import {SpeakerAffiliation, teams} from "../types/teamTypes";
import {useMyAdjudication} from "../util/hooks/useMyAdjudication";
import SpeakerEvaluation from "../components/evaluation/SpeakerEvaluation";
import TeamStrategyEvaluation from "../components/evaluation/TeamStrategyEvaluation";
import {Card} from "../components/basic/Card";
import {Divider} from "../components/basic/Divider";
import TeamInteractionEvaluation from "../components/evaluation/TeamInteractionEvaluation";
import TeamPersuasionEvaluation from "../components/evaluation/TeamPersuasionEvaluation";
import {LoadingDisplay} from "../components/LoadingDisplay";
import {SpeakerSorting} from "../components/SpeakerTab";
import {DebateAdjudication, SpeakerAdjudication} from "../types/states";
import {calculateSpeakerScore} from "../util/scores";
import {useContext, useState} from "react";
import {AppContext} from "../appContext";
import {Button} from "../components/basic/Button";

type EvaluationSpeakerListProps = {
  sorting: SpeakerSorting,
  adjudication: DebateAdjudication
}

type SpeakerAdjudicationWithAffiliation = SpeakerAdjudication & {
  affiliation: SpeakerAffiliation
}
export const EvaluationSpeakerList = ({
                                        sorting, adjudication,
                                      }: EvaluationSpeakerListProps) => {
  const {config} = useContext(AppContext)
  const speakerPositionSorting = (a: SpeakerAdjudication, b: SpeakerAdjudication) => a.position < b.position ? -1 : 1
  const govSpeeches: SpeakerAdjudicationWithAffiliation[] = [...adjudication["government"].speakers].sort(speakerPositionSorting).map(value => ({
    ...value,
    affiliation: "government"
  }))
  const oppSpeeches: SpeakerAdjudicationWithAffiliation[] = [...adjudication["opposition"].speakers].sort(speakerPositionSorting).map(value => ({
    ...value,
    affiliation: "opposition"
  }))
  const freeSpeeches: SpeakerAdjudicationWithAffiliation[] = [...adjudication["freeSpeeches"].speakers].sort(speakerPositionSorting).map(value => ({
    ...value,
    affiliation: "freeSpeeches"
  }))
  const teamSorting = [
    ...govSpeeches,
    ...oppSpeeches,
    ...freeSpeeches
  ]

  let usedSorting: SpeakerAdjudicationWithAffiliation[] = [];
  switch (sorting) {
    case "points":
      const scoreSorting = teamSorting.sort((a: SpeakerAdjudication, b: SpeakerAdjudication) => calculateSpeakerScore(a) > calculateSpeakerScore(b) ? -1 : 1)
      usedSorting = scoreSorting;
      break
    case  "team":
      usedSorting = teamSorting
      break
    case  "debateOrder":
      const debateOrderSorting: SpeakerAdjudicationWithAffiliation[] = []
      for (let i = 0; i < Math.max(config.teamSpeechCount - 1, 1); i++) {
        debateOrderSorting.push(govSpeeches[i])
        debateOrderSorting.push(oppSpeeches[i])
      }
      debateOrderSorting.push(...freeSpeeches)
      if (oppSpeeches.length > 0 && govSpeeches.length > 0) {
        debateOrderSorting.push(oppSpeeches[oppSpeeches.length - 1])
        debateOrderSorting.push(govSpeeches[govSpeeches.length - 1])
      }
      usedSorting = debateOrderSorting;
      break
  }


  return (
    <>
      {usedSorting.map(speaker => (
        <SpeakerEvaluation
          key={speaker.affiliation + speaker.position}
          affiliation={speaker.affiliation}
          position={speaker.position}
        />
      ))}
    </>
  )
}

/**
 * The EvaluationView display the averaged scores for all speakers and the team's team categories scores
 */
export const EvaluationView = () => {
  const adjudication = useMyAdjudication()
  const translation = useTranslation()
  const [sorting, setSorting] = useState<SpeakerSorting>("debateOrder")

  if (!adjudication) {
    return <LoadingDisplay/>
  }

  return (
    <ViewBase className={tw("gap-y-6")}>
      <Card
        title={(
          <div className={tw("flex flex-row gap-x-2 items-center")}>
            <Speech strokeWidth={2.5} size={36}/>
            <h2 id="speaker" className={tw("text-3xl font-bold")}>{translation.speaker}</h2>
          </div>
        )}
        expansionConfig={{isExpanded: true, enabled: true, showIcon: true}}
        className={tw("!p-0 !m-0 bg-transparent")}
      >
        <div className={tw("flex flex-col gap-y-4")}>
          <div className={tw("flex flex-row justify-between items-center mt-6 mb-2 w-full")}>
            <div className={tw("flex flex-row gap-x-2")}>
              <Button
                coloring={{color: sorting === "debateOrder" ? "positive" : "white"}}
                onClick={() => setSorting("debateOrder")}
                isRow={true}
              >
                {translation.debate}
              </Button>
              <Button
                coloring={{color: sorting === "team" ? "positive" : "white"}}
                onClick={() => setSorting("team")}
                isRow={true}
              >
                {translation.team}
              </Button>
              <Button
                coloring={{color: sorting === "points" ? "positive" : "white"}}
                onClick={() => setSorting("points")}
                isRow={true}
              >
                {translation.points}
              </Button>
            </div>
          </div>
          <EvaluationSpeakerList adjudication={adjudication} sorting={sorting}/>
        </div>
      </Card>
      <Divider/>
      <Card
        title={(
          <div className={tw("flex flex-row gap-x-2 items-center")}>
            <Presentation strokeWidth={2.5} size={36}/>
            <h2 id="strategy" className={tw("text-3xl font-bold")}>{translation.strategy}</h2>
          </div>
        )}
        expansionConfig={{isExpanded: true, enabled: true, showIcon: true}}
        className={tw("!p-0 !bg-transparent")}
      >
        <div className={tw("flex flex-col gap-y-4")}>
          {teams.map(affiliation => (
            <TeamStrategyEvaluation
              key={affiliation}
              affiliation={affiliation}
            />
          ))}
        </div>
      </Card>
      <Divider/>
      <Card
        title={(
          <div className={tw("flex flex-row gap-x-2 items-center")}>
            <MessageCircleQuestion strokeWidth={2.5} size={36}/>
            <h2 id="strategy" className={tw("text-3xl font-bold")}>{translation.interaction}</h2>
          </div>
        )}
        expansionConfig={{isExpanded: true, enabled: true, showIcon: true}}
        className={tw("!p-0 !bg-transparent")}
      >
        <div className={tw("flex flex-col gap-y-4")}>
          <div className={tw("flex flex-col gap-y-4")}>
            {teams.map(affiliation => (
              <TeamInteractionEvaluation
                key={affiliation}
                affiliation={affiliation}
              />
            ))}
          </div>
        </div>
      </Card>
      <Divider/>
      <Card
        title={(
          <div className={tw("flex flex-row gap-x-2 items-center")}>
            <BookHeart strokeWidth={2.5} size={36}/>
            <h2 id="strategy" className={tw("text-3xl font-bold")}>{translation.persuasion}</h2>
          </div>
        )}
        expansionConfig={{isExpanded: true, enabled: true, showIcon: true}}
        className={tw("!p-0 !bg-transparent")}
      >
        <div className={tw("flex flex-col gap-y-4")}>
          {teams.map(affiliation => (
            <TeamPersuasionEvaluation
              key={affiliation}
              affiliation={affiliation}
            />
          ))}
        </div>
      </Card>
    </ViewBase>
  )
}

export default EvaluationView;

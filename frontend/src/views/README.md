## Views

Views are different screens to show the user and are similar to different navigation routes.

The ViewHandler is the basic routing component handling the switching between the views by listening
for the active view in the AppContext

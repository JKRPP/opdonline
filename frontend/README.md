## Frontend

### Install and run

```shell
npm install
npm start
```

After that the server should run on [localhost:3000](http://localhost:3000/)

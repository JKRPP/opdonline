package io.hoppe.whohere.websocket

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.github.fge.jsonpatch.JsonPatch
import io.hoppe.whohere.state.SessionManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler
import org.springframework.web.util.UriComponentsBuilder
import java.time.LocalDateTime
import java.util.concurrent.ConcurrentHashMap

@Component
class WebSocketHandler() : TextWebSocketHandler() {

    @Autowired
    private lateinit var sessionManager: SessionManager
    private val mapper = ObjectMapper().registerModule(KotlinModule())
    private val partialMessageMap = ConcurrentHashMap<String, StringBuffer>()

    override fun handleTransportError(p0: WebSocketSession, p1: Throwable) {
    }

    override fun afterConnectionClosed(p0: WebSocketSession, p1: CloseStatus) {
        doForSession(p0) {
            it.sessions.remove(p0)
            val sessionId = getId(p0)
            if(it.sessions.size == 0 && sessionId != null) {
                it.lastActivity = LocalDateTime.now()
                GlobalScope.launch {
                    delay(86405000)
                    val lastClosedTime = it.lastActivity
                    if(lastClosedTime != null && lastClosedTime.isBefore(LocalDateTime.now().minusSeconds(86400000))) {
                        sessionManager.removeSession(sessionId)
                    }
                }
            }
        }
    }

    override fun handleMessage(p0: WebSocketSession, message: WebSocketMessage<*>) {
        synchronized(p0) {
            val payload = message.payload.toString()

            if (message.isLast) {
                val messageContent = if (partialMessageMap.containsKey(p0.id)) {
                    val buffer = partialMessageMap[p0.id]
                    partialMessageMap.remove(p0.id)
                    buffer?.append(payload).toString()
                } else {
                    payload
                }
                doForSession(p0, handleMessage(p0, messageContent))
            } else {
                if (!partialMessageMap.containsKey(p0.id)) {
                    partialMessageMap[p0.id] = StringBuffer()
                }
                partialMessageMap[p0.id]?.append(payload)
            }
        }
    }

    private fun handleMessage(p0: WebSocketSession, messageContent: String): (session: io.hoppe.whohere.state.Session) -> Unit {
        return { session ->
            session.lastActivity = LocalDateTime.now()
            val socketMessage = mapper.convertValue(messageContent, SocketMessage::class.java)
            when (socketMessage.type) {
                MessageType.INCREMENTAL_UPDATE -> {
                    synchronized(session) {
                        session.sessions.forEach {
                            //it.sendMessage(TextMessage(messageContent))
                            try {
                                it.sendMessage(TextMessage(messageContent));
                            } catch( ex: Exception ) {
                                synchronized( session ) {
                                    session.sessions.remove( it );
                                }
                            }
                        }
                        val patch = JsonPatch.fromJson(socketMessage.payload!!)
                        session.state = patch.apply(session.state) as ObjectNode
                    }
                }
                MessageType.STATE_UPDATE -> {
                    synchronized(session) {
                        session.sessions.forEach {
                            it.sendMessage(TextMessage(messageContent))
                        }
                        session.state = socketMessage.payload as ObjectNode
                    }
                }
                MessageType.STATE_REQUEST -> {
                    val resp = SocketMessage(MessageType.STATE_UPDATE, session.state)
                    p0.sendMessage(TextMessage(mapper.writeValueAsString(resp)))
                }
                MessageType.EVENT -> {
                    synchronized(session) {
                        session.sessions.forEach {
                            it.sendMessage(TextMessage(messageContent))
                        }
                    }
                }
            }
        }
    }

    override fun afterConnectionEstablished(p0: WebSocketSession) {
        doForSession(p0) {
            it.sessions.add(p0)
            it.lastActivity = LocalDateTime.now()
        }
    }

    private fun doForSession(ws: WebSocketSession, function: (session: io.hoppe.whohere.state.Session) -> Unit) {
        val sessionId = getId(ws)
        if(sessionId == null) {
            ws.sendMessage(TextMessage("{\"error\":\"session id not specified\"}"))
            return
        }

        if(sessionManager.hasSession(sessionId)) {
            function(sessionManager.getSession(sessionId)!!)
        }
    }

    private fun getId(ws: WebSocketSession): String? {
        val parameters = UriComponentsBuilder.fromUriString(ws.uri.toString()).build().queryParams
        return parameters["session"]?.get(0)
    }

    override fun supportsPartialMessages() = true
}
package io.hoppe.whohere.websocket

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode

data class SocketMessage(val type: MessageType, val payload: JsonNode? = null) {
    private companion object {
        private val mapper = ObjectMapper()
        @JsonCreator
        @JvmStatic
        fun valueOf(value: String): SocketMessage {
            val node = mapper.readValue(value, ObjectNode::class.java)
            val type = MessageType.valueOf(node["type"].asText())
            if(node.has("payload")) {
                return SocketMessage(type, node["payload"])
            } else {
                return SocketMessage(type)
            }
        }
    }
}

enum class MessageType {
    INCREMENTAL_UPDATE, STATE_UPDATE, STATE_REQUEST, EVENT
}

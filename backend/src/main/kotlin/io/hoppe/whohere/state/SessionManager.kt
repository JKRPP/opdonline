package io.hoppe.whohere.state

import org.springframework.web.bind.annotation.*
import java.lang.StringBuilder
import java.util.*
import kotlin.random.Random


@RestController
@CrossOrigin
class SessionManager {

    private val sessionMap = mutableMapOf<String, Session>()
    private val random = Random(System.currentTimeMillis())

    private val upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    @PostMapping("/api/v1/addSession")
    fun addSession(): String {
        var id = randomString(4)
        while(sessionMap.containsKey(id)) {
            id = randomString(4)
        }
        sessionMap[id] = Session()

        return id;
    }

    private fun randomString(length: Int): String {
        val sb = StringBuilder()
        for (i in 1..length) {
            sb.append(upper[random.nextInt(upper.length - 1)])
        }

        return sb.toString()
    }

    fun getSession(id: String) = sessionMap[id]

    @GetMapping("/api/v1/session/{id}")
    fun getState(@PathVariable("id") id: String) = sessionMap[id]?.state

    fun hasSession(sessionId: String) = sessionMap.containsKey(sessionId)

    fun removeSession(id: String) {
        getSession(id)?.sessions?.forEach { it.close() }
        sessionMap.remove(id)
    }
}

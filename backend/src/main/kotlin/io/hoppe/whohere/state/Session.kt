package io.hoppe.whohere.state

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.springframework.web.socket.WebSocketSession
import java.time.LocalDateTime

data class Session(
        var state: ObjectNode = ObjectMapper().createObjectNode(),
        val sessions: MutableList<WebSocketSession> = mutableListOf(),
        var lastActivity: LocalDateTime? = null)
package io.hoppe.whohere

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WhohereApplication

fun main(args: Array<String>) {
    runApplication<WhohereApplication>(*args)
}
